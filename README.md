[![Netlify Status](https://api.netlify.com/api/v1/badges/4a2362c0-2bf2-463c-922f-c0d87bc5d012/deploy-status)](https://app.netlify.com/sites/600watt-org/deploys)

# 600 Watt Website

Öffentliche URL: [www.600watt.org](https://www.600watt.org/)

## Entwicklung

Die Site wird mit HUGO erstellt. Darüber hinaus wird NodeJS benötigt und die NPM-Module `postcss` und `autoprefixer` müssen global installiert sein.

```nohighlight
npm install -g postcss postcss-cli autoprefixer
```

Zum Starten des lokalen Servers, als Vorschau während des Bearbeitens:

```nohighlight
hugo server --disableFastRender --ignoreCache --gc
```

Zum Rendern statischer Dateien:

```nohighlight
hugo --gc --minify
```

## Credits

Das Theme basiert auf [hugo-tailwindcss-starter-theme](https://github.com/dirkolbrich/hugo-tailwindcss-starter-theme), Copyright (c) 2018 Dirk Olbrich, veröffentlicht under MIT Lizenz.
