---
title: 600 Watt 📌 vor Ort
linkTitle: Vor Ort
description: Die Idee von 600 Watt ist, dass Solar-begeisterte sich in lokalen Gruppen gegenseitig helfen und zu Multiplikator:innen werden.
layout: single
weight: 20
---

Die Idee von 600 Watt ist, dass Solar-begeisterte sich in lokalen Gruppen gegenseitig helfen und zu Multiplikator:innen werden.

## Gruppen vor Ort

| Bundesland | Kreis/Ort | Links |
|-|-|-|
| Nordrhein-Westfalen | Rheinisch-Bergischer Kreis / Rösrath | [nebenan.de](https://nebenan.de/groups/50576) |

Zu **nebenan.de**: Um einer Gruppe beizutreten, benötigst Du ein Benutzerkonto und musst Mitglied in der lokalen Nachbarschaft sein.

**Erst eine einzige Gruppe?** Ja, genau. 600 Watt startet ja auch gerade erst.

Wenn Du eine 600 Watt Gruppe gründen und hier verlinken möchtest, [melde Dich]({{< relref "/kontakt" >}})!

## Andere lokale Gruppen {#andere}

Hier verlinken wir Gruppen mit ähnlichen Zielen.

| Bundesland | Kreis/Ort | Name |
|-|-|-|
| Baden-Württemberg | Freiburg im Breisgau | [Balkon.Solar](https://balkon.solar/)
| Bremen | Bremen | [Bremer SolidarStrom](https://bremer-solidarstrom.de/) |
| Hamburg | Hamburg | [Solisolar Hamburg](https://www.solisolar-hamburg.de/) |
| Hessen | Darmstadt-Dieburg/Otzberg | [Energiewende Otzberg](https://energiewende.otzberg.org/) |
| Nordrhein-Westfalen | Bochum | [Bochumer Klimaschutzbündnis (BoKlima)](https://boklima.de/) |
| Nordrhein-Westfalen | Dortmund | [Klimabündnis Dortmund, Energie AG](http://energie-ag.1megawatt.de/) |
