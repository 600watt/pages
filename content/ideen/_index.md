---
title: Unsere 💡 Ideen
linkTitle: Ideen
layout: single
weight: 10
---

Hier findest Du eine Übersicht unserer Ideen, wie wir Stecker-Solaranlagen fördern wollen.

- **Lokal handeln, überregional skalieren**: grundsätzlich erfordern viele Ideen hier lokales handeln, um möglichst viele Menschen zu erreichen. Daher sind viele Ideen so umzusetzen, dass sie möglichst leicht von anderen [lokalen Gruppen]({{< relref "/kontakt" >}}) übernommen werden können.

- **Website**: Jede lokale Gruppe sollte eine subdomain `ortsname.600watt.org` bekommen. Inhalte können aus einem Baukasten übernommen und angepasst werden.

- **Workshops** für alle, die eine Stecker-PV-Anlage haben wollen. Sollte sowohl in Präsenz vor Ort als auch online funktionieren.

- **Anlagenbesitzer:innen zu Multiplikator:innen machen** damit jede:r das gewonnene Wissen weiter gibt und so ein Schneeballeffekt entsteht.

- **Eine Karte**, auf der sich jede:r neue Anlagenbesitzer:in einträgt. Damit wird der Stolz auf das erreichte unterstrichen und der Erfolg wird nachvollziehbar. Und lokale Ansprechpartner:innen in der Nachrbarschaft können gefunden werden.

- **Erfahrungswerte**: Anlagenbesitzer:innen schreiben Berichte darüber, z. B. nach einem Jahr Betrieb, wie viel Ertrag ihnen die Anlage bringt und welche Herausforderungen sie bei der Installation überwinden mussten.

- **Gemeinsam einkaufen** und hoffentlich so günstigere Konditionen und verlässliche Lieferzeiten bei PV-Modulen, Wechselrichtern etc. aber auch bei Dienstleistern (Elektro-Fachbetriebe) erreichen. Einige Händler bieten keinen Versand. Da lohnt vielleicht eine Sammelbestellung, die durch die lokale Community organisiert werden kann.

- **Partnerschaft mit lokalen Betrieben** die mit Rat und Tat zur Seite stehen und eventuell freundliche Konditionen für Balkon-Anlagen bieten.

- **Einsammeln von ausrangierten PV-Modulen** die ansonsten entsorgt würden, zur Weiterverwendung in 600-Watt-Anlagen.

- **Öffentlichkeitsarbeit vor Ort**: Viele Menschen können am besten vor Ort erreicht werden, zum Beispiel bei Veranstaltungen. 600 Watt kann hier beispielsweise helfen, indem Inhalte und Gestaltung für Handzettel zur Verfügung gestellt werden.

- **Kooperation mit dem lokalen Stromnetzbetreiber** um beispielsweise bei Fragen zur Anmeldung von Anlagen zu vermitteln.

Wenn Du der Meinung bist, dass hier eine gute und wichtige Idee fehlt, [melde Dich]({{< relref "/kontakt" >}})!
