---
title: Welchen Unterschied macht der Wohnort?
description: Die Sonne scheint an verschiedenen Orten in Deutschland unterschiedlich stark. Aber wie groß ist der Unterschied?
weight: 100
author: Marian Steinbach
---

Vielleicht hast du schon mal gehört, dass der Süden Deutschlands sich für Photovoltaik besser eignet als der Norden. Das stimmt generell, bildet aber die Realität nicht genau ab. Hier wollen wir vor allem zeigen, wie groß die Unterschiede tatsächlich sind.

Schau dir dieses Diagramm an.

![Einstrahlung an 10 Orten in Deutschland](einstrahlung-senkrecht.svg)

Darin siehst du für zehn ausgewählte Städte in Deutschland, wie viel Sonnenenergie durchschnittlich pro Jahr auf eine senkrechte, nach Süden ausgerichtete Fläche von einem Quadratmeter fällt. Die Einheit hier ist Kilowattstunden. Grundlage für diese Daten ist [PVGIS-SARAH2](https://re.jrc.ec.europa.eu/pvg_tools/de/), ein globales Modell, das auf Messungen der Jahre 2005 bis 2020 basiert.

Der Ort mit der geringsten Globalstrahlung in unserer Auswahl ist Hamburg mit durchschnittlich 896 kWh pro Quadratmeter pro Jahr. Den höchsten Wert erreicht hier München mit 1015 kWh. Hamburg liegt damit 12 Prozent unter dem Wert von München.

## Was bedeutet das?

Die Globalstrahlung gibt an, wie stark die Sonne an einem Ort langfristig scheint. Das hat natürlich Auswirkungen auf den Ertrag einer Photovoltaikanlage.

Je nachdem wo in Deutschland du wohnst, wird deine Anlage langfristig einen unterschiedlich hohen Ertrag liefern. Bei ansonsten günstigen Bedingungen bedeutet das, dass sich deine Investition, verglichen mit einem idealen Standort, etwas langsamer amortisiert.

Andere Faktoren dürften jedoch deutlich stärkere Auswirkung haben. Wenn du beispielsweise deine Solarpanele mit einer optimalen Neigung montieren kannst, kannst Du den Nachteil der Globalstrahlung mehr als wettmachen.

![Einstrahlung bei verschiedenen Neigungen](einstrahlung-neigungen.svg)

Das Diagramm oben zeigt die Einstrahlung an den gleichen zehn Städten, aber bei verschiedenen Neigungen. Die gelben Säulen zeigen die gleichen Daten wie oben, also die senkrechte Fläche. In blaugrau siehst du zum Vergleich die Einstrahlung auf eine waagerechte/horizontale Fläche. Und die roten Säulen stellen die Einstrahlung für einen festen, optimalen Neigungswinkel dar.

Wie du siehst, fängt die optimal ausgerichtete Fläche in Hamburg deutlich mehr Energie ein, als die senkrecht ausgerichtete Fläche in München. Sogar mit einer waagerechten Montage (wie zum Beispiel flach auf einem Garagendach) kann man in Hamburg mehr Energie sammeln, als mit senkrecht montierten Panelen gleicher Größe in München.

Beachte das sowohl die senkrechte als auch die geneigte Fläche jeweils nach Süden ausgerichtet sind. Bei der waagerechten Fläche spielt die Ausrichtung keine Rolle.

## Fazit

Die Globalstrahlung am Ort ist nur ein Faktor, der den Ertrag deiner Anlage beeinflusst. Innerhalb Deutschlands sind die Unterschiede eher klein. Faktoren wie Neigung, Ausrichtung nach Süden und Schattenwurf dürften weit größere Auswirkung haben.

## Probier es selbst aus

Wenn du dich in ein wenig in das recht mächtige [PVGIS](https://re.jrc.ec.europa.eu/pvg_tools/de/) einarbeiten möchtest, kannst du dir die Daten für deinen Standort ansehen und sogar herausfinden, was der optimale Neigungswinkel für den maximalen Ertrag übers gesamte Jahr ist.

Für unseren Vergleich haben wir Code geschrieben, der in einem [Codeberg repository](https://codeberg.org/600watt/cities-radiation-comparison) zur freien Verfügung steht.
