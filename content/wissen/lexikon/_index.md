---
title: Lexikon
linkTitle: Lexikon
description: Fachbegriffe aus dem Bereich der Photovoltaik, einfach und verständlich erklärt.
weight: 10
layout: single
---

Hier erklären wir viele Begriffe, die rund um Stecker-Solar häufig fallen. Dabei wollen wir eine einfach verständliche Erklärung liefern und erlauben uns daher, einige Details außen vor zu lassen oder zu vereinfachen.

Du möchtest an dieser Seite mitwirken und Menschen helfen, Solarenergie und Photovoltaik besser zu verstehen? Du kannst [diese Seite auf Codeberg bearbeiten](https://codeberg.org/600watt/pages/src/branch/main/content/wissen/lexikon/_index.md).

---

**Inhalt**

{{< toc >}}

## Einspeisevergütung

Geld, das man erhält, wenn man Strom erzeugt und ins Netz einspeist. Die Einspeisevergütung ist im Erneuerbare-Energien-Gesetz (EEG) festgelegt. Dieses Gesetzt wurde mehrfach grundlegend geändert. Die Höhe der Vergütung hängt von vielen Faktoren ab, zum Beispiel von der Leistung der Anlage, und davon, ob man einen Teil des Stroms selbst nutzt.

Besitzer:innen von Stecker-Solaranlagen bis 600 Watt bemühen sich meist nicht, eine Vergütung für das Einspeisen ihres überschüssigen Stroms zu bekommen, weil der Aufwand sich nicht lohnt. Die Vergütung für das Einspeisen von Überschüssen beträgt nämlich aktuell nur 8,6 Cent pro Kilowattstunde. Dazu die [Verbraucherzentrale NRW](https://www.verbraucherzentrale.de/wissen/energie/erneuerbare-energien/steckersolar-solarstrom-vom-balkon-direkt-in-die-steckdose-44715#6):

> Bei Stecker-Solargeräten wird üblicherweise auch auf die EEG-Einspeisevergütung verzichtet. Wenn ein Stecker-Solargerät beispielsweise jährlich 500 Kilowattstunden Strom erzeugt und davon 150 Kilowattstunden ins Netz fließen, würde das eine Einspeisevergütung von rund 12 Euro im Jahr bedeuten. Dem stünde dann ein regelmäßiger Ablese- und Abrechnungsaufwand gegenüber.

## Marktstammdatenregister

Das [Marktstammdatenregister](https://www.marktstammdatenregister.de/MaStR) (MaStR) ist eine Sammlung mit Informationen zu allen Anlagen und deren Betreiber:innen im deutschen Energiesystem. Es wird durch die Bundesnetzagentur geführt.

Alle Photovoltaikanlagen in Deutschland, also auch die Steckergeräte bis 600 Watt, müssen ins Marktstammdatenregister eingetragen werden. Genaueres regelt die [Marktstammdatenregisterverordnung](https://www.gesetze-im-internet.de/mastrv/index.html).

## MPP-Tracking, MPPT {#mppt}

MPP steht für _Maximum Power Point_ und damit für die Optimierung der Leistungsausbeute einer Photovoltaikanlage. Dafür ist üblicherweise der [Wechselrichter](#wechselrichter) zuständig. [Mehr auf photovoltaik.org](https://www.photovoltaik.org/photovoltaikanlagen/wechselrichter/mpp-tracking)

## Netzbetreiber

Der Netzbetreiber, oder genauer, Verteilnetzbetreiber, ist ein Unternehmen, welches das Stromnetz betreibt, das vor Ort Elektrizität an Gebäude liefert. Unter Umständen betreibt dieses Unternehmen auch den Stromzähler, der eine Abrechnung des Stromverbrauchs und ggf. der Einspeisung ermöglicht.

Photovoltaikanlagen müssen beim Netzbetreiber, der für das lokale Verteilnetz zuständig ist, angemeldet werden. Wer das ist, kann man auf der Strom-Jahresrechnung oder dem Stromzähler ablesen.

## Netzparallelbetrieb

TODO

## Peak-Leistung

Auch "Spitzenleistung" genannt. Wert, der ausdrücken soll, wie viel Leistung ein Solarmodul bzw. eine Photovoltaikanlage maximal erbringen kann (von englisch "Peak" für "Gipfel"). Man findet die Angabe häufig mit einer Einheit wie "kWp" für "Kilowatt-Peak" vor.

Hersteller von Solarmodulen gehen bei ihren Angaben von genormten Bedingungen aus. Das bedeutet, dass Licht mit einem bestimmten Spektrum und einer bestimmten Energie in einem bestimmten Winkel auf das Solarmodul trifft, das eine bestimmte Temperatur haben muss. Die dabei gemessene Leistung ist die Peak-Leistung.

Das übliche Verfahren zur Ermittlung der Peak-Leistung heißt "STC" (für Standard Test Conditions). Hierbei wird Licht mit einer Intensität von 100 Watt je Quadratmeter genutzt und die Solarmodule müssen eine Temperatur von 25 Grad Celsius halten.

## Photovoltaik

Photovoltaik nennt man den Vorgang, die Energie aus Licht – in der Praxis: Sonnenlicht – für die Stromerzeugung zu nutzen. Dabei nutzt man den sogenannten Photoelektrischen Effekt, der schon seit 1839 bekannt ist. Unter anderem Albert Einstein erforschte diesen Effekt und bekam 1921 für seine Arbeit hierzu den Nobelpreis für Physik verliehen.

## Solarmodul

Solarmodul oder -panel nennt man Bauteile, die mehrere Solarzellen zu einem Bauteil verbinden. Dabei sind die einzelnen [Solarzellen](#solarzelle) mit einander in einer Reihenschaltung verbunden, so dass sich die Spannung der Zellen addiert. Das ist vergleichbar mit dem Aneinanderreihen von mehreren Batterien, wobei der Pluspol der einen Batterie den Minuspol der nächsten Batterie berührt.

Typische Größen von Solarmodulen enthalten 60 Zellen, haben Abmessungen von ca. 1,0 mal 1,7 Meter und bieten eine Maximalleistung von 300 bis 400 Watt. Aber es gibt auch Module mit anderen Abmessungen, Eigenschaften und Leistungswerten.

Die typische Steckverbindung für Solarmodule sind "MC4" Stecker bzw. Buchsen.

## Solarzelle

Eine Solarzelle ist ein Baustein, meist aus Silizium hergestellt, mit dessen Hilfe Sonnenlicht in Strom umgewandelt werden kann. Mehrere Solarzellen werden in einem [Solarmodul](#solarmodul) mit einander verbaut.

## Verschattung

Schattenwurf, oder auch Verschattung, ist ein Problem für Photovoltaikanlagen.

Eine Solaranlage, bei der oft mehrere Module in Reihe geschaltet sind, liefert normalerweise nur so viel Leistung, wie das schwächste Modul. Fällt Schatten auf ein Modul, sinkt die Leistung der gesamten Anlage so, als wären alle Module verschattet. Das gleiche gilt normalerweise für ein Solarmodul und die darin verbauten Zellen. Wird eine einzelne Zelle zur Hälfte verdeckt, zum Beispiel durch Laub, sinkt die Leistung des gesamten Moduls und damit der gesamten Anlage um die Hälfte.

Dort, wo beispielsweise Pflanzen oder Bäume regelmäßig Schatten auf einen Teil einer Photovoltaikanlage werfen, kann man entsprechende [Solarmodule](#solarmodul) einsetzen, die durch zusätzliche Bypassdioden weniger anfällig für Verschattung sind.

## Wechselrichter

Ein Wechselrichter ist ein Gerät, das den Gleichstrom, der von den Solarmodulen geliefert wird, in Wechselstrom umwandelt.

Wechselrichter, die in der Leistungsklasse bis 600 Watt eingesetzt werden, werden auch als Mikrowechselrichter oder Mikroinverter bezeichnet. Im Fall von "netzgeführten" und Anlagen, wozu die Balkon-Anlagen bis 600 Watt zählen, liefert der Wechselrichter nur dann Strom, wenn er an ein 230-Volt-Wechselstromnetz angeschlossen ist. Sobald die Verbindung zu diesem Netz unterbrochen wird, unterbricht der Wechselrichter die Stromlieferung. Falls der Wechselrichter über einen Schutzkontaktstecker ans Netz angeschlossen wird, wird hierdurch sicher gestellt, dass es nicht zu einem Stromschlag am Stecker kommen kann. Diese Vorrichtung nennt man auch Netz- und Anlagenschutz oder NA-Schutz. Näheres regelt die Norm VDE-AR-N 4105.

Dem Wechselrichter sollte beim Kauf eine Konformitätserklärung beiliegen, die erklärt, welche Standards das Gerät erfüllt.

## Wieland-Stecker

TODO

## Zweirichtungszähler

Ein Stromzähler, der zwei getrennte Zählfunktionen erfüllt. Zum einen zählt er aus dem Netz entnommenen Strom (Bezug). Zusätzlich zählt er die Einspeisung von produziertem Strom ins Netz.

Einzelne [Netzbetreiber](#netzbetreiber) bestehen darauf, dass beim Betrieb einer Photovoltaikanlage ein Zweirichtungszähler montiert wird. Andere verzichten darauf und machen es lediglich zur Auflage, dass der Stromzähler eine Rücklaufsperre besitzt, also nicht rückwärts zählen kann.
