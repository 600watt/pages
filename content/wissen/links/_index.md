---
title: Links
linkTitle: Links
description: Hier sammeln wir links zu guten Ressourcen im Netz.
weight: 90
layout: single
---

## Allgemeines

[Infoportal der Deutschen Gesellschaft für Sonnenenergie](https://www.pvplug.de/) mit Produktdatenbank und vielen Informationen für zukünftige Besitzer:innen von Steckersolaranlagen.

## Werkzeuge

[Stecker-Solar-Simulator](https://solar.htw-berlin.de/rechner/stecker-solar-simulator/) der Hochschule für Technik und Wirtschaft HTW Berlin. Erlaubt die Berechnung der Amortisation einer Steckersolaranlage mit vielen Parametern. Zum Beispiel: aktueller Stromverbrauch, Strompreis, Aufstellwinkel der Panele, Anzahl und Leistung der Panele und Anschaffungskosten.

[JRC Photovoltaic Geographical Information Sytem (PVGIS)](https://re.jrc.ec.europa.eu/pvg_tools/de/) ist ein komplexes Berechnungswerkzeug, zur Verfügung gestellt von der Europäischen Kommission, das sich an fortgeschrittene Anwender:innen richtet. Es kann für jeden beliebigen Ort den zu erwartenden Ertrag einer Photovoltaik-Anlage berechnen. Außerdem kannst Du damit herausfinden, welche Ausrichtung (Himmelsrichtung) und welche Neigung für Deinen Standort optimal wäre, also den größtmöglichen Ertrag übers gesamte Jahr liefern würde. Wenn Dein Standort von höheren Gebäuden umgeben ist, kannst Du sogar eine Beschreibung des Horizonts als Datei hochladen und damit den Schattenwurf mit einbeziehen.

## Recht

[PV Magazine: Amtsgericht Stuttgart schmettert Klage auf Rückbau von Balkonmodulen ab](https://www.pv-magazine.de/2021/09/15/amtsgericht-stuttgart-weist-klage-gegen-rueckbau-von-balkonmodulen-zurueck/) - Artikel über ein Urteil des Amtsgericht Stuttgart (Aktenzeichen 37 C 2283/20) vom März 2021. Eine Vermieterin hatte gegen ihren Mieter geklagt, um den Abbau seiner Solarmodule zu erzwingen. Das Gericht hat die Klage abgewiesen und vertrat die Auffassung, die Vermieterin hätte die Erlaubnis erteilen müssen.

## Studien

[Nutzung von Steckersolargeräten 2022](https://solar.htw-berlin.de/studien/nutzung-steckersolar-2022/) zu den Fragen: Wer sind die aktuellen Nutzer:innen und Interessierten? Wie werden Steckersolargeräte aktuell genutzt?  Aus welchen Motiven kaufen Nutzer:innen und Interessierte Steckersolargeräte? Welche Barrieren bestehen für potenzielle und tatsächliche Nutzer:innen bei Kauf und Nutzung von Steckersolar? Welchen Beitrag leisten ausgewählte Hilfestellungen (Tools) zur Über-
windung der Barrieren?

[Der Markt für Steckersolargeräte 2022](https://solar.htw-berlin.de/studien/marktstudie-steckersolar-2022/) – Studie der Hochschule für Technik und Wirtschaft HTW Berlin zum Absatz und Größenordnungen von Geräten.

## Wissen

Die Site mit dem obskuren Titel [PVCDROM](https://www.pveducation.org/pvcdrom/welcome-to-pvcdrom) ist eine unfassbare Wissenssammlung rund um Photovoltaik, herausgegeben vom Solar Power Lab der Arizona State University (ASU). Diese Sammlung ist für alle, die ganz tief in die Materie einsteigen, die physikalischen Zusammenhänge verstehen und sich das mathematische Handwerkszeug beispielsweise für eigene Simulationen aneignen wollen. Viele Darstellungen und Animationen, darunter auch interaktive, tragen dazu bei, dass Prinzipien und Zusammenhänge verständlicher werden.
