---
title: Förderung von Stecker-Solargeräten
linkTitle: Förderung
description: Liste von Förderprogrammen für Balkon-Photovoltaik. Meist lokal/regional beschränkt.
weight: 20
---

Diese Liste ist alles andere als vollständig. Bitte hilf mit, sie zu erweitern und [melde uns]({{< relref "/kontakt" >}}) weitere Programme oder [trage sie direkt selber ein](https://codeberg.org/600watt/pages/src/branch/main/content/wissen/foerderung/index.md).

| Bundesland | Kreis/Stadt/Gemeinde |
|-|-|
| Baden-Württemberg | [Freiburg](https://www.freiburg.de/pb/,Lde/232441.html) |
| Bayern | [Erlangen](https://erlangen.de/service/98103) |
| Mecklenburg-Vorpommern | [Gesamtes Bundesland](https://www.regierung-mv.de/Landesregierung/lm/Blickpunkte/faq-zur-foerderung-von-pv-anlagen) |
| Hessen | [Darmstadt](https://www.darmstadt.de/leben-in-darmstadt/klimaschutz/foerderprogramme/foerderprogramm-photovoltaik) |
| Niedersachsen | [Oldenburg](https://serviceportal.oldenburg.de/buergerservice/dienstleistungen/foerderprogramm-photovoltaik-900001066-36200.html) |
| Niedersachsen | [Stadt Vechta](https://www.vechta.de/news/alle-themen/nachricht/news/stadt-foerdert-mini-photovoltaikanlagen-fuer-mietswohnungen/) |
| Nordrhein-Westfalen | [Bonn](https://www.bonn.de/themen-entdecken/umwelt-natur/photovoltaik.php) |
| Nordrhein-Westfalen | [Köln](https://www.stadt-koeln.de/leben-in-koeln/klima-umwelt-tiere/klima/koeln-spezifische-massnahmenfoerderung-klimafreundliches-wohnen) |
| Nordrhein-Westfalen | [Regionalverband Ruhr](https://solarmetropole.ruhr/foerderungen-und-sonderaktionen/) |
| Nordrhein-Westfalen | [Rheinisch-Bergischer Kreis](https://www.rbk-direkt.de/solarfoerderung-auf-dem-weg-zum-solarkreis.aspx) |
| Thüringen | [Jena](https://umwelt.jena.de/de/foerderung-solarstromerzeugung) |
