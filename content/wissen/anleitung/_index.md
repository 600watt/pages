---
title: Schritt für Schritt zur eigenen Balkon-Solaranlage
linkTitle: Anleitung Schritt für Schritt
description: In unserer Anleitung erklären wir Schritt für Schritt, wie Du dir dein eigenes Balkonkraftwerk auswählen und installieren kannst.
weight: 10
layout: single
---

{{% note classes="warn text-sm" %}}

Diese Anleitung ist in Arbeit. Wenn Du dabei mithelfen möchtest, kannst gerne einen Pull Request zu unserem [repository](https://codeberg.org/600watt/pages) beisteuern oder ein [Issue](https://codeberg.org/600watt/pages/issues) erstellen, wenn Du einen Verbesserungswunsch hast.

{{% /note %}}

## Überblick

1. [Voraussetzungen klären und Standort bestimmen]({{< relref "/wissen/anleitung/schritt1-voraussetzungen" >}})
2. Erlaubnis einholen
3. Gerät beschaffen
4. Anlage montieren und anschließen
5. Anlage im Marktstammdatenregister anmelden
6. Anlage beim Netzbetreiber anmelden

## Einleitung

Mit dieser Anleitung wollen wir möglichst viele Menschen in die Lage versetzen, ihre eigene Balkon-Solaranlage zu planen und in Betrieb zu nehmen.

Das ist kein einfaches Unterfangen, denn es gibt dabei viele Dinge zu berücksichtigen. Und wie jede:r müssen auch wir klein anfangen. Wir werden uns anfangs auf das beschränken, was wir mit gutem Gewissen als richtig darstellen können. Dabei werden wir sicherlich einige Dinge unberücksichtigt lassen.

Wenn Du Anregungen zur Verbesserung dieser Anleitung hast, bitte [kontaktiere uns]({{< relref "/kontakt" >}}) und sag uns, was wir verbessern können.

Noch ein Hinweis, sozusagen als Gebrauchsanweisung zu dieser Anleitung. Lies Dir am besten die Anleitung erst mal komplett durch. Mach Dich erst danach an die Erledigung der einzelnen Aufgaben. Damit vermeidest Du vielleicht die eine oder andere Überraschung.

 → [Zu Schritt 1: Voraussetzungen klären und Standort bestimmen]({{< relref "/wissen/anleitung/schritt1-voraussetzungen" >}})