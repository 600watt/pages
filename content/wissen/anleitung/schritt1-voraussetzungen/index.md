---
title: "Schritt 1 – Voraussetzungen klären"
layout: single-with-toc
description: Im erster Schritt unserer Anleitung klären wir, ob bei dir alle Voraussetzungen für die Anschaffung und den Betrieb eines Balkon-Solargeräts gegeben sind.
enableScrollSpy: true
---

{{% note classes="warn text-sm" %}}

Diese Anleitung ist in Arbeit. Wenn Du dabei mithelfen möchtest, kannst gerne einen Pull Request zu unserem [repository](https://codeberg.org/600watt/pages) beisteuern oder ein [Issue](https://codeberg.org/600watt/pages/issues) erstellen, wenn Du einen Verbesserungswunsch hast.

{{% /note %}}

Hier geht es zuerst mal darum, herauszufinden, ob du tatsächlich eine eigene Balkon-Solaranlage betreiben kannst. Das hängt von einer Reihe von Faktoren ab, die unterschiedlich leicht einzuschätzen sind. Und manche beeinflussen sich auch noch gegenseitig. Aber schön der Reihe nach.

Beachte bitte, dass sich diese Inhalte auf 🇩🇪 Deutschland beziehen. In anderen Ländern können die Voraussetzung, insbesondere in Bezug auf die rechtliche Seite, abweichen.

## Habe ich einen geeigneten Platz? {#standort}

Wo soll die Anlage hin? Und wie viel Platz ist da? Ein typisches Solarmodul misst etwa 1 Meter mal 1,7 Meter.

Der Spitzname _Balkonkraftwerk_ kommt nicht von ungefähr. Denn ein (nahezu) idealer Platz ist die Außenseite einer **Balkonbrüstung**, wo sich je nach Größe des Balkons ein Modul oder mehrere Module nebeneinander anbringen lassen.

Aber nicht nur Balkone können sich als Standort eignen. Geeignet sind oft auch Garagen, Carports, Gärten, Gartenhäuschen oder Dächer von Anbauten. Solarmodule lassen sich theoretisch auch an eine Hauswand montieren, sofern man für die entsprechenden Halterungen sorgt.

Die **Ausrichtung** des Balkons bzw. der Anlage wirkt sich auf die Leistung und den Ertrag aus. In südlicher Richtung ist mit den höchsten Erträgen zu rechnen. Aber auch nach Westen oder Osten ausgerichtet kann sich die Anlage lohnen. Mit dem [Stecker-Solar-Simulator](https://solar.htw-berlin.de/rechner/stecker-solar-simulator/) kannst du das vorab einschätzen. Wenn bei Dir verschiedene Ausrichtungen in Frage kommen, kannst du im Simulator nacheinander die verschiedenen Ausrichtungen eingeben und die Ergebnisse miteinander vergleichen.

Der Standort der Anlage sollte frei von **Schattenwurf** sein. Wirft beispielsweise ein Gebäude oder ein Baum in den Mittagsstunden Schatten auf die Anlage, reduziert sich die Leistungsausbeute erheblich. Auch Schatten, die nur Teile eines Solarmoduls betreffen, wie beispielsweise von Pflanzen oder einem Laternenpfahl, wirken sich bei herkömmlichen Solarmodulen stark auf die Leistung aus. Falls sich teilweiser Schattenwurf bei Dir nicht vermeiden lässt, dann kannst du Dir überlegen, ob du die zusätzlichen Kosten für verschattungsresistente Solarmodule einplanen möchtest. Bei denen ist der Leistungsabfall im Fall einer teilweisen Abschattung nicht so stark wie bei herkömmlichen Modulen. Allerdings bieten nicht alle Hersteller solche Module an, die Beschaffung dürfte also schwieriger werden.

Zuletzt ist auch noch der **Stromanschluss** entscheidend. Hat dein Standort eine Außensteckdose? Dann ist die Anforderungen im Prinzip schon erfüllt. Ist keine Steckdose vorhanden, wird die Sache schwierig. Falls ein anderer Stromanschluss vorhanden ist, zum Beispiel für eine Außenbeleuchtung, solltest du mit einer Elektrofachkraft darüber sprechen, ob es möglich wäre, hier zusätzlich einen Anschluss für deine Photovoltaikanlage zu legen. Und natürlich kann man im Prinzip auch ein Stromkabel durch eine Außenwand führen. Das ist allerdings nichts, wobei wir Dich in dieser Anleitung unterstützen können.

Weil das Thema Stromanschluss noch ein bisschen komplizierter ist, haben wir dazu weiter unten bei [Wie möchte ich die Anlage anschließen?](#wie-anschliessen) einen eigenen Abschnitt.

Deine Standort-Checkliste:

&squ; Platz mit Ausrichtung zur Sonne, wenig Schatten  
&squ; Stromanschluss

## Welche Größenordnung soll die Anlage haben? {#dimension}

Die einfache Antwort könnte lauten: 600 Watt, denn das ist die Obergrenze der maßgeblichen Norm VDE-AR-N 4105. Solche Systeme bestehen üblicherweise aus zwei Standard-Solarmodulen und einem [Wechselrichter]({{< relref "/wissen/lexikon/#wechselrichter" >}}). Damit lassen sich bei optimalen Bedingungen bis zu 600 kWh Strom im Jahr erzeugen.

Was aber, wenn du weniger Platz hast, oder du weniger Geld ausgeben möchtest? Dann liegt es nah, nur ein Standard-Solarmodul zu nehmen. Da ein Modul maximal etwa 350 bis 400 Watt Leistung liefert, kann der Wechselrichter hier auch kleiner und günstiger ausfallen. Die zu erwartende Ausbeute halbiert sich ungefähr.

Mit dem [Stecker-Solar-Simulator](https://solar.htw-berlin.de/rechner/stecker-solar-simulator/) kannst du beide Varianten mit einander vergleichen.

Falls du Dich schon ein wenig umgehört und -gesehen hast, hast du vielleicht festgestellt, dass man bestimmte 600-Watt-Anlagen auch mit mehr als zwei Solarmodulen betreiben kann. Damit kann in den Zeiten, in denen die Sonne nicht maximal auf die Module strahlt, oder bei nicht optimaler Ausrichtung, die Leistung der Anlage insgesamt erhöht werden. Solange die Ausgangsleistung des Wechselrichters nicht mehr als 600 Watt beträgt, ist das mit der Norm vereinbar. Hierbei muss allerdings darauf geachtet werden, dass der Wechselrichter nicht überlastet wird.

Der oben verlinkte Simulator hilft Dir bei mehr als zwei Modulen leider nicht weiter. Wenn du aber auf derart fortgeschrittene Experimente aus bist, kannst du Dich eventuell mit [PVGIS](https://joint-research-centre.ec.europa.eu/pvgis-photovoltaic-geographical-information-system_en) vertraut machen. Dort kannst du eine beliebige Nennleistungen (Summe der kWp-Werte je Modul) der Anlage angeben.

Im übrigen sparen wir diese größeren Konstellationen mit mehr als zwei Solarmodulen im weiteren Verlauf dieser Anleitung aus, weil dies sonst die Erklärungen unnötig verkomplizieren würde.

Hier abschließend unsere **Zusammenfassung** für diesen Abschnitt:

- Wenn du genug Platz und Geld dafür hast, nimm eine Anlage mit zwei Modulen und 600-Watt-Wechselrichter.
- Ansonsten nimm eine Anlage mit nur einem Modul und entsprechend kleinerem Wechselrichter.

## Wie möchte ich die Anlage anschließen? {#wie-anschliessen}

Hier geht es um die scheinbar einfache Frage, wie die Anlage mit deinem Stromkreis verbunden werden soll. Wie oben schon angesprochen, spielen dabei natürlich die Gegebenheiten am Aufstellort eine Rolle. Aber nicht nur die. Auch die Bürokratie hat hier ein Wörtchen mitzureden.

In Deutschland gibt es eine große Kontroverse darüber, ob man ein Balkonkraftwerk an eine normale Steckdose (auch Schutzkontaktsteckdose oder Schuko-Steckdose) anschließen kann oder darf. Berichte zeigen, dass viele Betreiber:innen das genau so machen. Laut VDE Verband der Elektrotechnik Elektronik Informationstechnik steht das jedoch nicht im Einklang mit der Norm VDE-AR-N 4105, welche die Grundlage für den Betrieb eines Stromerzeugers bis 600 Watt am Stromnetz bietet. [Zitat](https://www.vde.com/de/fnn/arbeitsgebiete/tar/tar-niederspannung/erzeugungsanlagen-steckdose):

> Der Anschluss der Anlagen darf nur über eine spezielle Energiesteckvorrichtung unter Berücksichtigung der Anforderungen nach DIN VDE V 0100-551 und DIN VDE V 0100-551-1 erfolgen. Dann kann auch in vorhandene Endstromkreise eingespeist werden.

Diese _Energiesteckvorrichtung_ hat im deutschen Volksmund auch den Namen _Wieland-Stecker_ bzw. _Wieland-Steckdose_. Denn es gibt diese Vorrichtung offenbar nur von dieser einen Firma.

Die Deutsche Gesellschaft für Sonnenenergie (DGS) und die Verbraucherzentrale NRW beispielsweise betonen dagegen, dass Normen keine Gesetze sind und verweisen darauf, dass in anderen europäischen Ländern die Einspeisung über den Schuko-Stecker problemlos funktioniert.

Solltest du an Deinem Wohnort die Möglichkeit haben, eine Förderung für deine Anlage zu beantragen, dann sieh Dir die Bedingungen am besten frühzeitig an. Oft finden sich darin Formulierungen wie hier aus [Köln](https://www.stadt-koeln.de/mediaasset/content/pdf57/altbausanierung/stadt-koeln_richtlinie_foerderprogramm_gebaeudesanierung-und-erneuerbare-energien.pdf):

> Bestätigung einer fachgerechten Montage und eines fachgerechten elektrischen Anschlusses gemäß DIN VDE V 0100-551-1 durch einen Elektroinstallations-Fachbetrieb

Bei der ganzen Diskussion um die verschiedenen Stecker sollte nicht vergessen werden, dass ein fester Anschluss an den Stromkreis ebenfalls mit der Norm im Einklang steht und möglicherweise deutlich günstiger zu haben ist, als eine Wieland-Steckdose mit Stecker.

Als der-/diejenige, der/die zu dieser Frage eine Entscheidung fällen muss, hast du es angesichts der unterschiedlichen Positionen nicht leicht. Abnehmen können wir dir die Entscheidung allerdings nicht.

## Verkraftet unsere Elektroinstallation die Anlage? {#elektroinstallation}

Die Deutsche Gesellschaft für Sonnenenergie (DGS) rät zu einer Überprüfung durch eine:n Elektroinstallateur:in nur, falls keiner der beiden folgenden Fälle zutrifft ([Quelle](https://www.pvplug.de/faq/)):

- Der Stromkreis wird per Sicherungsautomat (Leitungsschutzschalter) abgesichert.

- Der Stromkreis wird per Schraubsicherung abgesichert und die vorhandene Sicherung wurde durch die nächst kleinere Sicherung ausgetauscht.

Laut Felduntersuchungen der DGS ist bei in Deutschland üblichen Elektroinstallationen davon auszugehen, dass Stecker-Solargeräte ohne Probleme angeschlossen werden können.

## Gibt es für den gewählten Platz eine geeignete Montage? {#montage}

Die üblichen Befestigungsmaterialien, wie sie auch von Händlern angeboten werden, welche Stecker-Solargeräte vertreiben, decken die gängigsten Fälle ab. Also Balkonbrüstungen, Dächer, Böden, und Wände.

Solltest du einen anderen Ort für die Anbringung in Betracht ziehen, dann kläre vorab, wie du die Module sicher befestigen kannst. Bedenke dabei, dass auf Solarpanele mit ihren mehr als 1,5 Quadratmetern Fläche bei Starkwind immense Kräfte wirken können.

## Deckt meine Haftpflichtversicherung eine solche Anlage? {#haftpflicht}

Niemand möchte davon ausgehen, dass dein Solargerät für irgendwen (oder irgendwas) zum Risiko wird. Aber für den Fall, dass eben doch etwas passiert, solltest du immerhin wissen, ob deine private Haftpflichversicherung die Anlage mit einschließt.

Eine Verbraucherzentrale schrieb und zu dieser Frage:

> Grundsätzlich sollte die private Haftpflichtversicherung Mietsachschäden beinhalten, damit eventuelle Schäden abgesichert sind. Ausschlüsse sind uns für diese Anlagen nicht bekannt.

Falls du trotzdem eine explizite Bestätigung von deiner Versicherungsgesellschaft wünschst, und sei es, weil beispielsweise deine Hausverwaltung oder dein:e Vermieter:in darum bittet, kannst du schriftlich eine Bestätigung anfordern. Hier ist eine Musteranfrage:

{{% note classes="text-sm" %}}
**Betrieb einer steckerfertigen PV-Anlage als Mieter**

Sehr geehrte Damen und Herren,

ich beabsichtige die Anschaffung und Inbetriebnahme einer steckerfertigen Photovoltaik-Anlage nach der Richtlinie VDE-AR-N 4105. Dabei handelt es sich um eine Solaranlage, die Strom bis ca. 600 Watt Spitzenleistung produziert und direkt am Stromkreis der Wohnung angeschlossen wird. Das Solarpanel bzw. die Panele sollen auf dem Balkon montiert werden.

Ich bitte Sie um eine schriftliche Bestätigung darüber, dass meine Versicherung (Nr. XXXXXXX) den Betrieb einer solchen Anlage mit einschließt.

Mit freundlichen Grüßen
{{% /note %}}

## Habe ich die Erlaubnis der Vermieter:in? {#vermieterin}

- bei Mehrfamilienhäusern: Eigentümerversammlung
- Bei Veränderungen wie Anbringung einer Außensteckdose ist eine explizite Erlaubnis einzuholen.

## Was sagen meine Nachbarn? {#nachbarn}

Mit den möchte man es sich nicht verscherzen. Würde dein Solarmodul Schatten auf den Balkon eines Nachbarn werfen, solltest du unbedingt vorher mit ihm/ihr darüber reden.

TODO: Tipps, wie man das Gespräch konstruktiv führen kann. Für die Idee begeistern.

## Kann ich mir die Investition leisten? {#kosten}

Wenn die wesentlichen Voraussetzungen, wie z. B. ein ertragreicher Standort, erfüllt sind, dann ist deine Balkon-PV-Anlage eine Investition, die sich auch finanziell rechnet. Denn wenn du tatsächlich Strom produzierst und selbst nutzt, und nicht gleichzeitig deinen Stromverbrauch an anderer Stelle steigerst, dann sparst du beim Stromverbrauch.

Bis sich die Investition amortisiert, wird es allerdings auch im idealen Fall einige Jahre dauern. Der schon erwähnte [Stecker-Solar-Simulator](https://solar.htw-berlin.de/rechner/stecker-solar-simulator/) erlaubt dir eine grobe Einschätzung.

Ob und wie schnell sich die Investition tatsächlich rentieren wird, hängt von ganz vielen Faktoren ab. Und es können unvorhergesehene Dinge Einfluss nehmen. Um nur einige zu nenne:

- Höhe deines Stromverbrauchs, vor allem tagsüber. Verbrauchst du mehr Strom, nutzt du potentiell auch mehr des selbst erzeugten Stroms und sparst entsprechend Strom vom Energieversorger.
- Höhe des Strompreises beim Energieversorger. Wie sich der entwickelt, kann niemand wirklich vorhersagen.
- Defekt. Dein Gerät könnte theoretisch aus irgendwelchen Gründen ausfallen. Technik ist leider nicht perfekt, und _shit happens_.
- Wohnortwechsel. Vielleicht musst du umziehen und hast am neuen Wohnort keine Sonne oder keine Erlaubnis, die Anlage zu betreiben? In diesem Fall wäre wohl ein Verkauf das sinnvollste.
- Ein Neubau in direkter Nachbarschaft sorgt plötzlich für Schatten.

Das alles zählen wir hier lediglich dafür auf, damit du eine möglichst fundierte Entscheidung treffen kannst. Insbesondere, wenn du das Geld für die Anlage nicht problemlos aufbringen kannst.

## Kann ich eine Förderung bekommen? {#foerderung}

Aktuell (Stand September 2022) gibt es in Deutschland nur lokal beschränkte Förderprogramme für Balkon-Solaranlagen. du musst Dich leider selbst auf die Suche machen, weil wir hier momentan keine Übersicht liefern können.

Falls es ein Förderprogramm für Deinen Wohnort gibt, sieh Dir in jedem Fall die Bedingungen genau an und überleg Dir, ob du diese erfüllen kannst.
