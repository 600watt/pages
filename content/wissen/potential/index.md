---
title: Wie groß ist das Potential von Balkon-Photovoltaik in Deutschland?
description: Können kleine Solaranlagen auf Balkonen einen nennenswerten Beitrag zur Energiewende leisten? Hier versuchen wir, zu beziffern, wie viele solche Anlagen es geben könnte.
weight: 200
author: Marian Steinbach
image: potential-stecker-solar-deutschland.png
---

Können kleine Solaranlagen auf Balkonen einen nennenswerten Beitrag zur Energiewende leisten? Hier versuchen wir, zu beziffern, wie viele solche Anlagen es geben könnte.

## Ausgangspunkt

In einer [Marktstudie](https://solar.htw-berlin.de/studien/marktstudie-steckersolar-2022/) kommt die HTW Berlin zusammen mit der Verbraucherzentrale NRW zu der Schätzung, dass bis Ende 2021 in Deutschland etwa 140.000 bis 190.000 Stecker-Solaranlagen verkauft wurden. Das entspricht einer Leistung von etwa 59 bis 66 Megawatt.

Daraus ergibt sich eine durchschnittliche Nennleistung je Anlage zwischen 310 und 471 Watt.

## Potential

In Deutschland gibt es etwa 41 Millionen Wohnungen. Davon sind ca. 28 Millionen in Mehrfamilienhäusern ([Stand Ende 2021](https://www.destatis.de/DE/Themen/Gesellschaft-Umwelt/Wohnen/_inhalt.html)).

Wir wissen nicht, wie gut diese Wohnungen tatsächlich für die Montage von Stecker-Solargeräten geeignet sind. Nehmen wir aber an, dass nur jede zehnte Wohnung geeignet wäre, ergäbe das ein Potential von 2,8 Millionen Anlagen.

Den Wert für die durchschnittliche Nennleistung der Anlagen übernehmen wir in etwa aus der oben genannten Marktstudie und runden auf 400 Watt.

2,8 Millionen Anlagen mit durchschnittlich 400 Watt Nennleistung würden eine insgesamt 1.120 Megawatt (oder 1,12 Gigawatt) Nennleistung ergeben.

![Abbildung Potential Balkon-Solar in Deutschland](potential-stecker-solar-deutschland.png)

## Zum Vergleich

Die Nennleistung aller installierten Photovoltaikanlagen in Deutschland ([Stand März 2022](https://www.destatis.de/DE/Presse/Pressemitteilungen/2022/06/PD22_N037_43.html)) beträgt 58.400 Megawatt (oder 58,4 Gigawatt).

Eine durchschnittliche Onshore-Windkraftanlage in Deutschland hat eine Nennleistung von ziemlich genau 2 Megawatt ([Quelle: BWE](https://www.wind-energie.de/themen/zahlen-und-fakten/deutschland/)). 1,12 Gigawatt Balkon-Solar entsprechen 563 Onshore-Windkraftanlagen.

Die Nennleistung des Atomkraftwerks Isar 2 beträgt etwa 1,5 Gigawatt.
