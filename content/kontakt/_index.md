---
title: 💌 Kontakt
linkTitle: Kontakt
layout: single
weight: 100
---

Du interessierst Dich für 600 Watt und möchtest Menschen helfen, ihren eigenen Solarstrom zu erzeugen? So kannst Du Dich melden:

### Kontaktmöglichkeiten

Fediverse: [@600watt@mastodon.green](https://mastodon.green/@600watt)

E-Mail: info At 600watt Punkt org (in Kleinbuchstaben)

Auf unserer [Projektseite auf Codeberg.org](https://codeberg.org/600watt/Hauptquartier/issues) kannst Du öffentlich Fragen stellen oder Anregungen hinterlassen.

### Verantwortlich

Marian Steinbach, Im Pannenhack 40a, 51503 Rösrath

Telefon und Signal: [+49 179 5 91 70 86](tel:00491795917086)
